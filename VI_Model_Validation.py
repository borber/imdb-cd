import pandas as pd
from functools import partial

from III_Data_Preparation import data_preparation
from IV_Model_Training import wg_trainer
from V_Model_Evaluation import get_accuracy

def model_validation():
    df = pd.read_csv('model_path.csv')
    df['validation_accuracy'] = df['model'].apply(get_accuracy)
    print(df)
    if df['validation_accuracy'].iloc[-1] == max(df['validation_accuracy']):
        tmp = df['model'].iloc[-1][:-12]
        run_name = tmp[tmp.rfind('/')+1:]
        trainer.mlflow_register(run_name=run_name)
        

if __name__ == '__main__':
    
    ds = data_preparation()['test']
    get_accuracy = partial(get_accuracy, ds=ds)
    args = {'data_seed':888, 
            'batch_size': 1, 
            'eval_steps': 120, 
            'model_name': "w11wo/javanese-roberta-small-imdb-classifier", 
            'patience': 3, 
            'output_dir': "/mlflow/runs/IMDB",
            'test_mode': True,
            'dataset_name': 'imdb',
            'exp_name': 'IMDB',
            }
    trainer = wg_trainer(**args)
    model_validation()