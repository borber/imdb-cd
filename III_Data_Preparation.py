from datasets import load_dataset
from transformers import AutoTokenizer
def data_preparation(model_name="w11wo/javanese-roberta-small-imdb-classifier"):
    ds = load_dataset('./data/raw_data')
    def tokenize(examples):
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        outputs = tokenizer(examples['text'], truncation=True)
        return outputs
    ds = ds.map(tokenize, batched=True)
    return ds

if __name__ == '__main__':
    data_preparation()