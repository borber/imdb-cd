import jenkins
import time
# It comes with pip module python-jenkins
# use pip to install python-jenkins

# Jenkins Authentication URL
JENKINS_URL = "http://172.30.234.99:8080"
JENKINS_USERNAME = "lulu"
JENKINS_PASSWORD = "1688"


class DevOpsJenkins:
    def __init__(self):
        self.jenkins_server = jenkins.Jenkins(JENKINS_URL, username=JENKINS_USERNAME, password=JENKINS_PASSWORD)
        user = self.jenkins_server.get_whoami()
        version = self.jenkins_server.get_version()
        print ("Jenkins Version: {}".format(version))
        print ("Jenkins User: {}".format(user['id']))

    def build_job(self, name, parameters=None, token=None):
        next_build_number = self.jenkins_server.get_job_info(name)['nextBuildNumber']
        # self.jenkins_server.build_job(name, parameters=parameters, token=token)
        self.jenkins_server.build_job(name, token=token)
        time.sleep(10)
        build_info = self.jenkins_server.get_build_info(name, next_build_number)
        return build_info


def build_job(test_mode=False,
              exp_name="IMDB",
              NAME_OF_JOB="Registered-Model-API",
              TOKEN_NAME="inference",
              PARAMETERS={'project': 'devops'},
              ):
    if test_mode:
        exp_name = 'test_' + exp_name
    jenkins_obj = DevOpsJenkins()
    output = jenkins_obj.build_job(NAME_OF_JOB, PARAMETERS, TOKEN_NAME)
    print ("Jenkins Build URL: {}".format(output['url']))

if __name__ == '__main__':
    build_job()