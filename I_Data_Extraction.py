from datasets import load_dataset
import pandas as pd
import os
def data_extraction():
    split = ['train[:1%]', 'test[:1%]', 'test[1%:2%]']
    ds = load_dataset("imdb", split=split)
    os.makedirs('data/raw_data', exist_ok=True)
    pd.DataFrame(ds[0]).to_csv('data/raw_data/raw_train.csv', index=False)
    pd.DataFrame(ds[1]).to_csv('data/raw_data/raw_val.csv', index=False)
    pd.DataFrame(ds[2]).to_csv('data/raw_data/raw_test.csv', index=False)

if __name__ == '__main__':
    data_extraction()