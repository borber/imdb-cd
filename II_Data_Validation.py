import pandas as pd
import re
import os
def data_validation():
    df = pd.concat([pd.read_csv(f'data/raw_data/{x}') for x in os.listdir('data/raw_data')], axis=0)
    df['length'] = df['text'].apply(lambda x: len(x))
    pattern = r"[^\w\s]"  # This pattern matches anything that is not a word character or whitespace
    df['symbols_ratio'] = df['text'].apply(lambda x: len(re.findall(pattern, x)))
    df['symbols_ratio'] = df['symbols_ratio'] / df['length']
    df[['length', 'symbols_ratio']].describe().to_csv('data/data_description.csv')
    
if __name__ == '__main__':
    data_validation()